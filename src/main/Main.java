package main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Main {
    static JFrame jFrame = getFrame();
    static JPanel jPanel = new JPanel();
    static JButton currentColorButton;
    static Color currentColor = Color.BLACK;


    public static void main(String[] args) {
        jFrame.add(jPanel);


        JToolBar colorBar = new JToolBar("ColorBar", JToolBar.HORIZONTAL);
        currentColorButton = new JButton();
        currentColorButton.setBackground(currentColor);
        currentColorButton.setToolTipText("Выбранный цвет");
        colorBar.add(currentColorButton);
        colorBar.addSeparator();

        JButton blackColorButton = new JButton();
        blackColorButton.setBackground(Color.BLACK);
        blackColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentColor = Color.BLACK;
                currentColorButton.setBackground(currentColor);
            }
        });
        colorBar.add(blackColorButton);

        JButton redColorButton = new JButton();
        redColorButton.setBackground(Color.RED);
        redColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentColor = Color.RED;
                currentColorButton.setBackground(currentColor);
            }
        });
        colorBar.add(redColorButton);

        JButton orangeColorButton = new JButton();
        orangeColorButton.setBackground(Color.ORANGE);
        orangeColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentColor = Color.ORANGE;
                currentColorButton.setBackground(currentColor);
            }
        });
        colorBar.add(orangeColorButton);

        JButton yellowColorButton = new JButton();
        yellowColorButton.setBackground(Color.YELLOW);
        yellowColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentColor = Color.YELLOW;
                currentColorButton.setBackground(currentColor);
            }
        });
        colorBar.add(yellowColorButton);

        JButton greenColorButton = new JButton();
        greenColorButton.setBackground(Color.GREEN);
        greenColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentColor = Color.GREEN;
                currentColorButton.setBackground(currentColor);
            }
        });
        colorBar.add(greenColorButton);

        JButton cyanColorButton = new JButton();
        cyanColorButton.setBackground(Color.CYAN);
        cyanColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentColor = Color.CYAN;
                currentColorButton.setBackground(currentColor);
            }
        });
        colorBar.add(cyanColorButton);

        JButton blueColorButton = new JButton();
        blueColorButton.setBackground(Color.BLUE);
        blueColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentColor = Color.BLUE;
                currentColorButton.setBackground(currentColor);
            }
        });
        colorBar.add(blueColorButton);

        JButton magentaColorButton = new JButton();
        magentaColorButton.setBackground(Color.MAGENTA);
        magentaColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentColor = Color.MAGENTA;
                currentColorButton.setBackground(currentColor);
            }
        });
        colorBar.add(magentaColorButton);

        JButton whiteColorButton = new JButton();
        whiteColorButton.setBackground(Color.WHITE);
        whiteColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentColor = Color.WHITE;
                currentColorButton.setBackground(currentColor);
            }
        });
        colorBar.add(whiteColorButton);

        jFrame.add(colorBar);

        JToolBar jToolBar = new JToolBar("Tool Bar", JToolBar.HORIZONTAL);
        JMenuBar jMenuBar = new JMenuBar();

        JMenu rectangle = new JMenu("Rectangle");
        jMenuBar.add(rectangle);
        JButton emptyRectangle = new JButton("emptyRectangle");
        emptyRectangle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jPanel.addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {

                    }

                    @Override
                    public void mousePressed(MouseEvent e) {

                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {

                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {

                    }

                    @Override
                    public void mouseExited(MouseEvent e) {

                    }
                });
            }
        });

        JButton filledRectangle = new JButton("filledRectangle");
        filledRectangle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        rectangle.add(emptyRectangle);
        rectangle.add(filledRectangle);

        JMenu ellipse = new JMenu("Ellipse");
        jMenuBar.add(ellipse);
        JButton emptyEllipse = new JButton("emptyEllipse");
        emptyEllipse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        JButton filledEllipse = new JButton("filledEllipse");
        filledEllipse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        ellipse.add(emptyEllipse);
        ellipse.add(filledEllipse);

        jToolBar.add(jMenuBar);
        JButton lineButton = new JButton();
        jToolBar.add(lineButton);
        jToolBar.setBounds(0,0,30,300);
        jToolBar.setVisible(true);
        jFrame.add(jToolBar);




    }

    static JFrame getFrame(){
        JFrame jFrame = new JFrame("Practice Paint");
        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setSize(800, 600);
        jFrame.setLocationRelativeTo(null);
        return jFrame;
    }
}